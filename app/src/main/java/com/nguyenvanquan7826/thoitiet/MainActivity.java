package com.nguyenvanquan7826.thoitiet;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Temperater> temperaterList = new ArrayList<>();
    private ArrayAdapter<Temperater> adapter;

    private ListView lvTemp;

    private EditText editDay;
    private EditText editTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindView();
    }

    private void bindView() {
        editDay = findViewById(R.id.editDay);
        editTemp = findViewById(R.id.editTemp);

        lvTemp = findViewById(R.id.lvTemp);

        adapter = new ArrayAdapter<Temperater>(this, 0, temperaterList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_temp, null);

                TextView tvDay = convertView.findViewById(R.id.tvDay);
                TextView tvTemp = convertView.findViewById(R.id.tvTemp);
                ImageView ivTemp = convertView.findViewById(R.id.ivTemp);

                Temperater t = temperaterList.get(position);

                tvDay.setText(t.getDay());
                tvTemp.setText(String.valueOf(t.getTemp()));

                int idIv = R.drawable.ic_mua;
                if (t.getTemp() < 10) idIv = R.drawable.ic_mua;
                else if (t.getTemp() < 20) idIv = R.drawable.ic_may;
                else idIv = R.drawable.ic_nang;

                ivTemp.setImageResource(idIv);

                return convertView;
            }
        };
        lvTemp.setAdapter(adapter);

        findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTemp();
            }
        });

        lvTemp.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showInfo(position);
                return false;
            }
        });
    }

    private void addTemp() {
        String day = editDay.getText().toString().trim();
        String temp = editTemp.getText().toString().trim();

        if(TextUtils.isEmpty(day)) {
            toast(getString(R.string.error_day));
            return;
        }

        if(TextUtils.isEmpty(temp)) {
            toast(getString(R.string.error_temp));
            return;
        }

        int tempNum = Integer.parseInt(temp);

        Temperater t = new Temperater();
        t.setDay(day);
        t.setTemp(tempNum);

        temperaterList.add(t);
        adapter.notifyDataSetChanged();
    }

    private void showInfo(int position){
        Temperater t = temperaterList.get(position);

        Intent i = new Intent(this, InfoActivity.class);
        i.putExtra(InfoActivity.KEY_DAY, t.getDay());
        i.putExtra(InfoActivity.KEY_TEMP, t.getTemp());
        startActivity(i);
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
