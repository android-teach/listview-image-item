package com.nguyenvanquan7826.thoitiet;

public class Temperater {
    private String day;
    private int temp;

    public String getDay() {
        return day;
    }

    public Temperater setDay(String day) {
        this.day = day;
        return this;
    }

    public int getTemp() {
        return temp;
    }

    public Temperater setTemp(int temp) {
        this.temp = temp;
        return this;
    }
}
