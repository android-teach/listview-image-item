package com.nguyenvanquan7826.thoitiet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    public static final String KEY_DAY = "day";
    public static final String KEY_TEMP = "temp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        String day = getIntent().getStringExtra(KEY_DAY);
        int temp = getIntent().getIntExtra(KEY_TEMP, 0);

        TextView tvInfo = findViewById(R.id.tvInfo);
        tvInfo.setText(day + " - " + temp);

    }

}
